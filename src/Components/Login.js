import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./Login.css"
import { MdAlternateEmail} from 'react-icons/md';
import { ImKey} from 'react-icons/im';
import Vert from "./Vert.png"





  function Login() {
    return (
      <div className="container ">
        <div className="row ">
          <div className="col-lg-6 col1 mt-5 text-center ">
            <h1 className="text-uppercase title1 mb-5 text-center">Connectez-vous</h1>
            <div className="form ">
            <div class="input-group mb-4 ">
  <div class="input-group-prepend">
    <span className="input-group-text icon groupInput" ><MdAlternateEmail/></span>
  </div>
  <input type="text" class="form-control groupInput" placeholder="Address Mail " aria-label="Username" aria-describedby="basic-addon1"/>
</div>
<div class="input-group mb-4">
  <div class="input-group-prepend">
    <span class="input-group-text icon groupInput" id="basic-addon1"><ImKey/></span>
  </div>
  <input type="text" class="form-control groupInput" placeholder="Mot de passe" aria-label="Username" aria-describedby="basic-addon1"/>
</div>
<p className="para">Mot de passe oublié?</p>
<div className="buton mb-4 groupInput ">
<button type="submit" className="btn text-uppercase  ">connexion</button>

</div>
</div>
    
          </div>

          <div className="col-lg-6 mt-5  col2 text-center">
          <img  className="image" src={Vert} />
          <h1 className="text-uppercase title pt-3">mixte feewi</h1>
          </div>


        </div>
      </div>
    );
  }
  export default Login;